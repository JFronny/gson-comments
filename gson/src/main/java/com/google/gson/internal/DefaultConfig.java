package com.google.gson.internal;

import com.google.gson.*;
import com.google.gson.reflect.*;

public class DefaultConfig {
    public static final boolean DEFAULT_JSON_NON_EXECUTABLE = false;
    public static final Strictness DEFAULT_STRICTNESS = Strictness.LEGACY_STRICT;
    public static final boolean DEFAULT_OMIT_QUOTES = false;
    public static final FormattingStyle DEFAULT_FORMATTING_STYLE = FormattingStyle.COMPACT;
    public static final boolean DEFAULT_ESCAPE_HTML = true;
    public static final boolean DEFAULT_SERIALIZE_NULLS = false;
    public static final boolean DEFAULT_COMPLEX_MAP_KEYS = false;
    public static final boolean DEFAULT_DUPLICATE_MAP_KEYS = false;
    public static final boolean DEFAULT_SPECIALIZE_FLOAT_VALUES = false;
    public static final boolean DEFAULT_USE_JDK_UNSAFE = true;
    public static final String DEFAULT_DATE_PATTERN = null;
    public static final FieldNamingStrategy DEFAULT_FIELD_NAMING_STRATEGY = FieldNamingPolicy.IDENTITY;
    public static final ToNumberStrategy DEFAULT_OBJECT_TO_NUMBER_STRATEGY = ToNumberPolicy.DOUBLE;
    public static final ToNumberStrategy DEFAULT_NUMBER_TO_NUMBER_STRATEGY = ToNumberPolicy.LAZILY_PARSED_NUMBER;
    public static final TypeToken<?> NULL_KEY_SURROGATE = TypeToken.get(Object.class);
    public static final String JSON_NON_EXECUTABLE_PREFIX = ")]}'\n";
}
