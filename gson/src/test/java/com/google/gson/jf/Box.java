package com.google.gson.jf;

import java.util.List;

public class Box {
  public static class StringArray {
    public String[] collection;
  }

  public static class StringList {
    public List<String> collection;

    public static StringList of(String... list) {
      StringList sl = new StringList();
      sl.collection = List.of(list);
      return sl;
    }
  }

  public static class IntList {
    public List<Integer> collection;
  }
}
