package com.google.gson.jf;

import com.google.gson.Gson;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class Json5Test {
    @Test
    public void testShortExample() {
        // Taken from the official example
        assertThat(new Gson().fromJson("{\n" +
                "  // comments\n" +
                "  unquoted: 'and you can quote me on that',\n" +
                "  singleQuotes: 'I can use \"double quotes\" here',\n" +
                "  lineBreaks: \"Look, Mom! \\\n" +
                "No \\\\n's!\",\n" +
                "  hexadecimal: 0xdecaf,\n" +
                "  leadingDecimalPoint: .8675309, andTrailing: 8675309.,\n" +
                "  positiveSign: +1,\n" +
                "  trailingComma: 'in objects', andIn: ['arrays',],\n" +
                "  \"backwardsCompatible\": \"with JSON\"\n" + // The original includes a trailing comma here, that is not yet supported
                "}", ExampleModel.class).isDefault())
                .isTrue();
    }

    public static class ExampleModel {
        public String unquoted;
        public String singleQuotes;
        public String lineBreaks;
        public Integer hexadecimal;
        public Double leadingDecimalPoint;
        public Double andTrailing;
        public Integer positiveSign;
        public String trailingComma;
        public String[] andIn;
        public String backwardsCompatible;

        public boolean isDefault() {
            return unquoted.equals("and you can quote me on that")
                    && singleQuotes.equals("I can use \"double quotes\" here")
                    && lineBreaks.equals("Look, Mom! \nNo \\n's!")
                    && hexadecimal.equals(0xdecaf)
                    && leadingDecimalPoint.equals(.8675309)
                    && andTrailing.equals(8675309.)
                    && positiveSign.equals(1)
                    && trailingComma.equals("in objects")
                    && andIn.length == 1 && andIn[0].equals("arrays")
                    && backwardsCompatible.equals("with JSON");

        }
    }
}
