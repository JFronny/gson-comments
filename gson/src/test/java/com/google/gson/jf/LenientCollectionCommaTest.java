package com.google.gson.jf;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.Strictness;
import org.junit.Test;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class LenientCollectionCommaTest {
  private Gson lenient = new GsonBuilder().setStrictness(Strictness.LENIENT).create();
  private static final String json = "{\"collection\": [1, 2,, 3, , , 4, 5,]}";

  @Test
  public void testElementSkipping() {
    assertThat(lenient.fromJson(json, Box.IntList.class).collection)
            .isEqualTo(List.of(1, 2, 3, 4, 5));
  }
}
