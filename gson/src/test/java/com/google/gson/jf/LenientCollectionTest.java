package com.google.gson.jf;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.Strictness;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class LenientCollectionTest {
  private Gson lenient = new GsonBuilder().setStrictness(Strictness.LENIENT).create();
  private Gson strict = new GsonBuilder().create();
  private static final String json = "{\"collection\": \"example\"}";

  @Test
  public void testLenientArray() {
    assertThat(lenient.fromJson(json, Box.StringArray.class).collection)
            .isEqualTo(new String[] {"example"});
  }

  @Test
  public void testLenientList() {
    assertThat(lenient.fromJson(json, Box.StringList.class).collection)
            .isEqualTo(List.of("example"));
  }

  @Test
  public void testStrictArray() throws NoSuchFieldException, IllegalAccessException {
    assertThrows("Strict Gson should not deserialize array without brackets",
            JsonParseException.class,
            () -> strict.fromJson(json, Box.StringArray.class));
  }

  @Test
  public void testStrictList() {
    assertThrows("Strict Gson should not deserialize list without brackets",
            JsonParseException.class,
            () -> strict.fromJson(json, Box.StringList.class));
  }
}
