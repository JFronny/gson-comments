package com.google.gson.jf;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.Strictness;
import com.google.gson.stream.JsonReader;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import static com.google.common.truth.Truth.assertThat;

public class ReadNullTest {
    @Test
    public void testReadNull() throws IOException {
        Gson gson = new GsonBuilder().serializeNulls().setStrictness(Strictness.LENIENT).create();
        String exampleFile = "{\n  // Yes\n  \"value1\": 1024,\n  \"value2\": null,\n  \"value3\": 10\n}";
        try (Reader r = new StringReader(exampleFile); JsonReader jr = gson.newJsonReader(r)) {
            jr.beginObject();
            assertThat(jr.nextName()).isEqualTo("value1");
            assertThat(jr.nextInt()).isEqualTo(1024);
            assertThat(jr.nextName()).isEqualTo("value2");
            jr.nextNull();
            assertThat(jr.nextName()).isEqualTo("value3");
            assertThat(jr.nextInt()).isEqualTo(10);
            jr.endObject();
        }
    }
}
