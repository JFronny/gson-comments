package com.google.gson.jf;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class OmitQuotesTest {
    private Gson lenient = new GsonBuilder().setOmitQuotes().create();
    private Gson strict = new GsonBuilder().create();
    private static final String lenientJson = "{collection:[\"example\"]}";
    private static final String strictJson = "{\"collection\":[\"example\"]}";
    private Box.StringList slb = Box.StringList.of("example");

    @Test
    public void testLenient() {
        assertThat(lenient.toJson(slb)).isEqualTo(lenientJson);
    }

    @Test
    public void testStrict() {
        assertThat(strict.toJson(slb)).isEqualTo(strictJson);
    }
}
